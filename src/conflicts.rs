use crossterm::AlternateScreen;
use crossterm_input::{input, InputEvent, KeyEvent};
use std::collections::{HashMap, HashSet};
use std::io;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;
use tui::backend::CrosstermBackend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, SelectableList, Widget};
use tui::Terminal;

enum Event<I> {
    Input(I),
    Tick,
}

fn get_conflicts(base_index: usize, entries: &Vec<(String, HashSet<String>)>) -> Vec<String> {
    let mut conflicts = vec![];
    for (index, (other_key, other_entries)) in entries.iter().enumerate() {
        if index != base_index {
            let intersection = other_entries & &entries[base_index].1;
            let mut cur_conflicts: Vec<&String> = intersection.iter().collect();
            cur_conflicts.sort();
            if !cur_conflicts.is_empty() {
                if base_index > index {
                    conflicts.push(format!("Higher - {}:\n", other_key));
                } else {
                    conflicts.push(format!("Lower - {}:\n", other_key));
                }
                for file in cur_conflicts {
                    conflicts.push(format!("    {}", file));
                }
            }
        }
    }
    return conflicts;
}

pub fn conflicts(
    left_title: &str,
    right_title: &str,
    entries: &Vec<(String, HashSet<String>)>,
) -> Result<(), io::Error> {
    // Initialize Terminal
    let screen = AlternateScreen::to_alternate(true)?;
    let backend = CrosstermBackend::with_alternate_screen(screen)?;
    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;

    let keys: Vec<&String> = entries.iter().map(|(x, _)| x).collect();

    let mut selected_left = 0;
    let mut selected_right = None;

    let (tx, rx) = mpsc::channel();
    {
        let tx = tx.clone();
        thread::spawn(move || {
            let input = input();
            let reader = input.read_sync();
            for event in reader {
                match event {
                    InputEvent::Keyboard(key) => {
                        if let Err(_) = tx.send(Event::Input(key.clone())) {
                            return;
                        }
                        if key == KeyEvent::Char('q') {
                            return;
                        }
                    }
                    _ => {}
                }
            }
        });
    }
    {
        let tx = tx.clone();
        thread::spawn(move || {
            let tx = tx.clone();
            loop {
                tx.send(Event::Tick).unwrap();
                thread::sleep(Duration::from_millis(250));
            }
        });
    }

    terminal.clear()?;
    let mut cache: HashMap<usize, Vec<String>> = HashMap::new();

    fn move_up(start: usize, amount: usize, max: usize) -> usize {
        if start >= amount {
            start - amount
        } else if start == max - 1 && max < amount {
            0
        } else {
            max - 1
        }
    }

    fn move_down(start: usize, amount: usize, max: usize) -> usize {
        if start == 0 && max < amount {
            max - 1
        } else if start + amount > max - 1 {
            0
        } else {
            start + amount
        }
    }

    loop {
        terminal.draw(|mut f| {
            let chunks = Layout::default()
                .direction(Direction::Horizontal)
                .margin(1)
                .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
                .split(f.size());

            let style = Style::default().fg(Color::White).bg(Color::Black);
            SelectableList::default()
                .block(Block::default().borders(Borders::ALL).title(left_title))
                .items(&keys)
                .select(Some(selected_left))
                .style(style)
                .highlight_style(style.fg(Color::LightGreen).modifier(Modifier::BOLD))
                .highlight_symbol(">")
                .render(&mut f, chunks[0]);
            if !cache.contains_key(&selected_left) {
                cache.insert(selected_left, get_conflicts(selected_left, entries));
            }
            SelectableList::default()
                .block(Block::default().borders(Borders::ALL).title(right_title))
                .items(&cache.get(&selected_left).unwrap())
                .select(selected_right)
                .style(style)
                .highlight_style(style.fg(Color::LightGreen).modifier(Modifier::BOLD))
                .highlight_symbol(">")
                .render(&mut f, chunks[1]);
        })?;
        match rx.recv() {
            Ok(Event::Input(event)) => match event {
                KeyEvent::Esc | KeyEvent::Char('q') => {
                    break;
                }
                KeyEvent::Left | KeyEvent::Char('a') | KeyEvent::Char('h') => {
                    selected_right = None;
                }
                KeyEvent::Down | KeyEvent::Char('s') | KeyEvent::Char('j') => {
                    if selected_right.is_none() {
                        selected_left = move_down(selected_left, 1, keys.len());
                    } else {
                        selected_right = Some(move_down(
                            selected_right.unwrap(),
                            1,
                            cache.get(&selected_left).unwrap().len(),
                        ));
                    }
                }
                KeyEvent::Right | KeyEvent::Char('d') | KeyEvent::Char('l') => {
                    selected_right = Some(0);
                }
                KeyEvent::Up | KeyEvent::Char('w') | KeyEvent::Char('k') => {
                    if selected_right.is_none() {
                        selected_left = move_up(selected_left, 1, keys.len());
                    } else {
                        selected_right = Some(move_up(
                            selected_right.unwrap(),
                            1,
                            cache.get(&selected_left).unwrap().len(),
                        ));
                    }
                }
                KeyEvent::PageUp => {
                    let term_height = terminal.size()?.height;
                    if selected_right.is_none() {
                        selected_left =
                            move_up(selected_left, (term_height / 2) as usize, keys.len());
                    } else {
                        selected_right = Some(move_up(
                            selected_right.unwrap(),
                            (term_height / 2) as usize,
                            cache.get(&selected_left).unwrap().len(),
                        ));
                    }
                }
                KeyEvent::PageDown => {
                    let term_height = terminal.size()?.height;
                    if selected_right.is_none() {
                        selected_left =
                            move_down(selected_left, (term_height / 2) as usize, keys.len());
                    } else {
                        selected_right = Some(move_down(
                            selected_right.unwrap(),
                            (term_height / 2) as usize,
                            cache.get(&selected_left).unwrap().len(),
                        ));
                    }
                }
                _ => {}
            },
            Ok(Event::Tick) => {}
            Err(e) => {
                panic!("{:?}", e);
            }
        }
    }

    Ok(())
}
